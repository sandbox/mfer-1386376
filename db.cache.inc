<?php
/**
 * @file
 * The database cache in core doesn't respect ssl/https for html. This fixes that.
 *
 * @author Matt Farina (mfer)
 */

/**
 * This class uses the core Database caching mechanism but provides some
 * alterations to the cache id (cid) for some tables to respect https/ssl.
 *
 * This class takes the approach of opting out of the https identifier for ids
 * on a per tables basis rather than opting in to it. The idea is to provide a
 * wide coverage for cases where contrib adds it's own tables.
 */
class DrupalDatabaseCacheResepectHTTPS extends DrupalDatabaseCache {

  protected $_whitelist_defaults = array(
    'cache_page',       // This table already respects https by containing the full url.
    'cache_path',       // Caching of aliases for a page is relative.
    'cache_menu',       // The internal paths we are concerned with are relative as stored here.
    'cache_image',      // This one looks safe too.
    'cache_bootstrap',  // Shouldn't be html in this cache.
    'cache_views',
    'cache_views_data',
    //'cache',            // This may not be safe to whitelist as many modules may use this cache poorly.
  );

  protected $_is_on_whitelist = NULL;

  function __construct($bin) {
    parent::__construct($bin);
    
    // If this bin is on the whitelist (doesn't need https signification) store
    // that information.
    $whitelist = variable_get('database_cache_respecting_https_whitelist', $this->_whitelist_defaults);
    if (in_array($bin, $whitelist)) {
      $this->_is_on_whitelist = TRUE;
    }
    else {
      $this->_is_on_whitelist = FALSE;
    }
  }

  public function get($cid) {
    $cache = parent::get($this->_httpsifycid($cid));

    // We transform the cid back to it's original value in case some calling code
    // checks the id. This could cause problems is stored with https at the start
    // and lead to uncached stuff where we expect it cached.
    if ($cache) {
      $cache->cid = $cid;
    }

    return $cache;
  }

  public function getMultiple(&$cids) {

    // If on the whilelist remove the complications imposed here.
    if ($this->_is_on_whitelist) {
      return parent::getMultiple($cids);
    }

    $new_cids = array();
    $cid_map = array();

    foreach ($cids as $k => $v) {
      $new_cid =  $this->_httpsifycid($v);
      $new_cids[] = $new_cid;
      $cid_map[$new_cid] = $v;
    }

    $parentcache = parent::getMultiple($new_cids);

    // Some code (such as fields) looks at the cids from the returned material
    // and acts on it. We need to transform cids back to the ones passed in.
    // This is trickery and indirection. It's fugly.
    $cache = array();
    foreach ($parentcache as $k => $v) {
      $v->cid = $cid_map[$k];
      $cache[$cid_map[$k]] = $v;
    }

    return $cache;
  }

  public function set($cid, $data, $expire = CACHE_PERMANENT) {
    return parent::set($this->_httpsifycid($cid), $data, $expire);
  }

  public function clear($cid = NULL, $wildcard = FALSE) {
    if (!$this->_is_on_whitelist && !is_null($cid)) {

      // If the cid and https version of the cid are different we want to clear
      // both caches when one of them is cleared.
      $ncid = $this->_httpsifycid($cid);
      if ($ncid != $cid) {
        parent::clear($cid, $wildcard);
      }
      $cid = $ncid;
    }
    return parent::clear($cid, $wildcard);
  }

  /**
   * Tweak a cache id (cid) so that it includes https info if it was cached under
   * https.
   *
   * @param string $cid
   *  A cache id (cid) used for setting and getting cache items.
   *
   * @return string
   *  An updated cacheid with https in the name if it was recorded under https.
   */
  protected function _httpsifycid($cid) {

    if (!$this->_is_on_whitelist) {
      global $is_https;

      // We have https: checking at the stop to make sure we don't end up with
      // multiple things marking an item. Or to not get into some form of nesting.
      if ($is_https  && substr($cid, 0, 6) != 'https:') {
        // prepending https to the beginning so it's easy to spot when taking a
        // look at the cache through a UI.
        $cid = 'https:' . $cid;
      }
    }

    return $cid;
  }
}